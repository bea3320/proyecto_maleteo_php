<?php

namespace App\Controller;

use App\Entity\Opiniones;
use App\Entity\Solicitud;
use App\Form\OpinionesForm;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class MaleteoController extends AbstractController
{
    /**
     * @Route("/maleteo", name="maleteo")
     */

    public function homePage()
    {
        return $this->render("base.html.twig");
    }

    /**
     * @Route("/maleteo/datos")
     */

    public function newDateForm(Request $request, EntityManagerInterface $doctrine)
    {
        $nombre = $request->get('nombre');
        $email = $request->get('email');
        $ciudad = $request->get('ciudad');

        $data = new Solicitud();
        $data->setNombre($nombre);
        $data->setEmail($email);
        $data->setCiudad($ciudad);

        $doctrine->persist($data);
        $doctrine->flush();

        return $this->redirectToRoute("maleteo");
    }

    /**
     * @Route("/maleteo/solicitudes")
     */

    public function listarDemo(EntityManagerInterface $doctrine)
    {
        $demo = $doctrine->getRepository(Solicitud::class)->findAll();
        return $this->render(
            "list-demo.html.twig",
            [
                'solicitud' => $demo
            ]
        );

    }

    /**
     * @Route("/insert/opiniones")
     */

    public function insertOpiniones(EntityManagerInterface $doctrine)
    {
        $mario = new Opiniones();
        $mario->setComentario("Lorem, ipsum dolor sit amet consectetur adipisicing elit. Laborum est atque modi doloremque, nesciunt saepe voluptas ipsa aliquam nam laboriosam similique, quo ad sint ut veniam nostrum, cupiditate dignissimos. Inventore");
        $mario->setNombre("Mario");
        $mario->setCiudad("Madrid");

        $ainhoa = new Opiniones();
        $ainhoa->setComentario("Lorem, ipsum dolor sit amet consectetur adipisicing elit. Laborum est atque modi doloremque");
        $ainhoa->setNombre("Ainhoa");
        $ainhoa->setCiudad("Córdoba");

        $diego = new Opiniones();
        $diego->setComentario("Lorem, ipsum dolor sit amet consectetur adipisicing elit. Laborum est atque modi doloremque, nesciunt saepe voluptas ipsa aliquam");
        $diego->setNombre("Diego");
        $diego->setCiudad("Badajoz");

        $ruben = new Opiniones();
        $ruben->setComentario("Lorem, ipsum dolor sit amet consectetur adipisicing elit. Laborum est atque modi doloremque");
        $ruben->setNombre("Ruben");
        $ruben->setCiudad("Asturias");

        $sandra = new Opiniones();
        $sandra->setComentario("Lorem, ipsum dolor sit amet consectetur adipisicing elit. Laborum est atque modi doloremque, nesciunt saepe voluptas ipsa aliquam");
        $sandra->setNombre("Sandra");
        $sandra->setCiudad("Valencia");

        $doctrine->persist($mario);
        $doctrine->persist($ainhoa);
        $doctrine->persist($diego);
        $doctrine->persist($ruben);
        $doctrine->persist($sandra);

        $doctrine->flush();

        return $this->render("opiniones.html.twig");

    }

    /**
     * @Route("/maleteo/opinionesForm")
     */

    public function opinionesForm(Request $request, EntityManagerInterface $doctrine)
    {
        return $this->render("new-form.html.twig");
        }

    

    /**
     * @Route("/maleteo/opinionesForm/nuevo")
     */

    public function opinionesFormulario(Request $request, EntityManagerInterface $doctrine)
    {

        $formOpinion = $this->createForm(OpinionesForm::class);
        $formOpinion->handleRequest($request);

        IF($formOpinion->isSubmitted() && $formOpinion->isValid()){
            $opinion = $formOpinion->getData();

            $doctrine->persist($opinion);
            $doctrine->flush();
        }
        return $this->render(
            "new-formulario.html.twig",
            [
                'OpinionForm' => $formOpinion->createView()
            ]
        );
    }

    /**
     * @Route("/opiniones")
     */

    public function opiniones(EntityManagerInterface $doctrine)
    {
        $repo = $doctrine->getRepository(Opiniones::class);

        $opiniones = $repo->findAll();

        return $this->render("opiniones.html.twig", ["opiniones"=>$opiniones]);
    }
}

